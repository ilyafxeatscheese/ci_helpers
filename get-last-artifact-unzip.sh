FILTER=".[] | select(.status==\"success\")  | select(.name==\"$1\") | select(.ref==\"$5\")"
LAST_SUCCESSFUL_BUILD=$(curl -s -H "PRIVATE-TOKEN: $3" "https://gitlab.com/api/v4/projects/$2/jobs?per_page=50" | jq -c ''"$FILTER"' | {id}' | head -n1 | grep -oE '[0-9]+')
curl -fksSL -o $4 -H "PRIVATE-TOKEN: $3" "https://gitlab.com/api/v4/projects/$2/jobs/${LAST_SUCCESSFUL_BUILD}/artifacts"
unzip $4
